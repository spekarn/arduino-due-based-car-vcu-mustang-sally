//Librarys for screen
#include <SPI.h>
#include <SD.h>
#include "Adafruit_GFX.h"
#include "Adafruit_HX8357.h"
#include <Fonts/FreeMonoBoldOblique12pt7b.h>

//Screen - These are 'flexible' lines that can be changed
#define TFT_CS 50 //SPI Chip Select Pin
#define TFT_DC 51 //SPI Data / Command Pin
#define TFT_RST 52 // RST can be set to -1 if you tie it to Arduino's reset
#define SD_CS 53 // SD Card Chip Select

#define BLACK    0x0000
#define BLUE     0x001F
#define RED      0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0
#define WHITE    0xFFFF

// Use hardware SPI (on Uno, #13, #12, #11) and the above for CS/DC
Adafruit_HX8357 tft = Adafruit_HX8357(TFT_CS, TFT_DC, TFT_RST);

#define RELAY_ON 0 //8-channel Solid State Relay Board - Relay is "active low"
#define RELAY_OFF 1

// Arduino Digital I/O pin number //Funktionen const int funkar �ven tror jag.
#define Relay_ACC1  24
#define Relay_ACC2  25
#define Relay_Ignition1  26
#define Relay_Ignition2  27
#define Relay_Beam  28
#define Relay_Reverse_Light  29
#define Relay_Indicator_LR  30
#define Relay_Indicator_RR  31
#define Relay_Start  32
#define Relay_Horn  33
#define Relay_Indicator_LF  34
#define Relay_Indicator_RF  35

bool Switch_ACC = 0;
bool Switch_Ignition = 0;
bool Switch_Park = 0;
bool Switch_Beam = 0;
bool Switch_Brake = 0;
bool Switch_Indicator_L = 0;
bool Switch_Indicator_R = 0;
bool Switch_Hazard_Indicator = 0;
bool Switch_Start = 0;
bool Switch_Horn = 0;
bool Switch_Reverse = 0;

//for blinker
unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 500;           // interval at which to blink (milliseconds)
int indicatorState = HIGH;

int state = 0; // State of Serial3.Read / Should be char? -127 +127 for BT module.

int loopdelay = 1; //loopdelay to change in setup (default 10ms)

unsigned int currentMillis = 0;
unsigned int lpc_display = 0;
unsigned int loopcount = 0;

// added variables for debounce
bool Switch_ACC_db = 0;
bool last_Switch_ACC = 0;
unsigned int lpc_Switch_ACC = 0;
unsigned int lpc_check_Switch_ACC = 0;

bool Switch_Ignition_db = 0;
bool last_Switch_Ignition = 0;
unsigned int lpc_Switch_Ignition = 0;
unsigned int lpc_check_Switch_Ignition = 0;

bool Switch_Park_db = 0;
bool last_Switch_Park = 0;
unsigned int lpc_Switch_Park = 0;
unsigned int lpc_check_Switch_Park = 0;

bool Switch_Beam_db = 0;
bool last_Switch_Beam = 0;
unsigned int lpc_Switch_Beam = 0;
unsigned int lpc_check_Switch_Beam = 0;

bool Switch_Brake_db = 0;
bool last_Switch_Brake = 0;
unsigned int lpc_Switch_Brake = 0;
unsigned int lpc_check_Switch_Brake = 0;

bool Switch_Indicator_L_db = 0;
bool last_Switch_Indicator_L = 0;
unsigned int lpc_Switch_Indicator_L = 0;
unsigned int lpc_check_Switch_Indicator_L = 0;

bool Switch_Indicator_R_db = 0;
bool last_Switch_Indicator_R = 0;
unsigned int lpc_Switch_Indicator_R = 0;
unsigned int lpc_check_Switch_Indicator_R = 0;

bool Switch_Hazard_Indicator_db = 0;
bool last_Switch_Hazard_Indicator = 0;
unsigned int lpc_Switch_Hazard_Indicator = 0;
unsigned int lpc_check_Switch_Hazard_Indicator = 0;

bool Switch_Start_db = 0;
bool last_Switch_Start = 0;
unsigned int lpc_Switch_Start = 0;
unsigned int lpc_check_Switch_Start = 0;

bool Switch_Horn_db = 0;
bool last_Switch_Horn = 0;
unsigned int lpc_Switch_Horn = 0;
unsigned int lpc_check_Switch_Horn = 0;

bool Switch_Reverse_db = 0;
bool last_Switch_Reverse = 0;
unsigned int lpc_Switch_Reverse = 0;
unsigned int lpc_check_Switch_Reverse = 0;

void setup()   /****** SETUP: RUNS ONCE ******/
{
  delay(500); //delay to allow time for tft display startup.. just an idea
  Serial.begin(9600);
  //Serial3.begin(9600); // Default connection rate for BT module

  loopdelay = 1; //loopdelay to change in setup (default 10ms)

  //For screen  
  tft.begin(HX8357D);
  tft.setRotation(3);
  tft.fillScreen(HX8357_BLACK);

  //---( THEN set pins as outputs )----

  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);
  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  pinMode(11, INPUT);
  pinMode(12, INPUT);

  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);
  pinMode(28, OUTPUT);
  pinMode(29, OUTPUT);
  pinMode(30, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(35, OUTPUT);

  digitalWrite(Relay_ACC1, RELAY_OFF);
  digitalWrite(Relay_ACC2, RELAY_OFF);
  digitalWrite(Relay_Ignition1, RELAY_OFF);
  digitalWrite(Relay_Ignition2, RELAY_OFF);
  digitalWrite(Relay_Beam, RELAY_OFF);
  digitalWrite(Relay_Reverse_Light, RELAY_OFF);
  digitalWrite(Relay_Indicator_LR, RELAY_OFF);
  digitalWrite(Relay_Indicator_RR, RELAY_OFF);
  digitalWrite(Relay_Start, RELAY_OFF);
  digitalWrite(Relay_Horn, RELAY_OFF);
  digitalWrite(Relay_Indicator_LF, RELAY_OFF);
  digitalWrite(Relay_Indicator_RF, RELAY_OFF);

	//For screen

	tft.fillScreen(HX8357_BLACK);

	tft.setFont(&FreeMonoBoldOblique12pt7b);
	tft.setCursor(150, 25);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Mustang Sally");

	tft.drawLine(35, 35, 450, 35, BLUE); //x,y,x,y,color // from xy to xy

	tft.setFont();
	tft.setCursor(160, 60);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(2);
	tft.print("DIAGNOS");

	tft.setCursor(10, 80);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_ACC:");

	tft.setCursor(10, 90);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Ignition:");

	tft.setCursor(10, 100);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Park:");

	tft.setCursor(10, 110);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Beam:");

	tft.setCursor(10, 120);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Brake:");

	tft.setCursor(10, 120);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Indicator_L:");

	tft.setCursor(10, 130);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Indicator_R:");

	tft.setCursor(10, 140);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Hazard:");

	tft.setCursor(10, 150);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Start:");

	tft.setCursor(10, 150);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Horn:");

	tft.setCursor(10, 160);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Switch_Reverse:");

	tft.setCursor(10, 190);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_ACC1:");

	tft.setCursor(10, 200);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_ACC2:");

	tft.setCursor(10, 210);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Ignition1:");

	tft.setCursor(10, 220);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Ignition2:");

	tft.setCursor(10, 230);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Beam:");

	tft.setCursor(10, 240);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Reverse_Light:");

	tft.setCursor(10, 250);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Indicator_LR:");

	tft.setCursor(10, 260);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Indicator_RR:");

	tft.setCursor(10, 270);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Start:");

	tft.setCursor(10, 280);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Horn:");

	tft.setCursor(10, 290);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Indicator_LF:");

	tft.setCursor(10, 300);
	tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
	tft.print("Relay_Indicator_RF:");

}//--(end setup )---
void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  currentMillis = millis(); //Counts time since arduino startup in milliseconds
  loopcount++ ;
  Switch_ACC = digitalRead(2);
  Switch_Ignition = digitalRead(3);
  Switch_Park = digitalRead(4);
  Switch_Beam = digitalRead(5);
  Switch_Brake = digitalRead(6);
  Switch_Indicator_L = digitalRead(7);
  Switch_Indicator_R = digitalRead(8);
  Switch_Hazard_Indicator = digitalRead(9);
  Switch_Start = digitalRead(10);
  Switch_Horn = digitalRead(11);
  Switch_Reverse = digitalRead(12);

  // ACC debounce
  if (Switch_ACC != last_Switch_ACC) {
    lpc_Switch_ACC ++;
    if (lpc_Switch_ACC == 1) {
      lpc_check_Switch_ACC = loopcount;
    }

    if (lpc_Switch_ACC > 5) { //not 5 subsequent 
      lpc_Switch_ACC = 0;
    }
    if ((lpc_Switch_ACC == 5) && ((loopcount - lpc_check_Switch_ACC) == 4)) {
      Switch_ACC_db = Switch_ACC;
      lpc_Switch_ACC = 0;
      last_Switch_ACC = Switch_ACC;
    }
  }

  // Ignition debounce
  if (Switch_Ignition != last_Switch_Ignition) {
    lpc_Switch_Ignition ++;
    if (lpc_Switch_Ignition == 1) {
      lpc_check_Switch_Ignition = loopcount;
    }

    if (lpc_Switch_Ignition > 5) {  //not 5 subsequent 
      lpc_Switch_Ignition = 0;
    }
    if ((lpc_Switch_Ignition == 5) && ((loopcount - lpc_check_Switch_Ignition) == 4)) {
      Switch_Ignition_db = Switch_Ignition;
      lpc_Switch_Ignition = 0;
      last_Switch_Ignition = Switch_Ignition;
    }
  }

  // Park debounce
  if (Switch_Park != last_Switch_Park) {
    lpc_Switch_Park ++;
    if (lpc_Switch_Park == 1) {
      lpc_check_Switch_Park = loopcount;
    }

    if (lpc_Switch_Park > 5) {  //not 5 subsequent 
      lpc_Switch_Park = 0;
    }
    if ((lpc_Switch_Park == 5) && ((loopcount - lpc_check_Switch_Park) == 4)) {
      Switch_Park_db = Switch_Park;
      lpc_Switch_Park = 0;
      last_Switch_Park = Switch_Park;
    }
  }

  // Beam debounce
  if (Switch_Beam != last_Switch_Beam) {
    lpc_Switch_Beam ++;
    if (lpc_Switch_Beam == 1) {
      lpc_check_Switch_Beam = loopcount;
    }

    if (lpc_Switch_Beam > 5) {  //not 5 subsequent 
      lpc_Switch_Beam = 0;
    }
    if ((lpc_Switch_Beam == 5) && ((loopcount - lpc_check_Switch_Beam) == 4)) {
      Switch_Beam_db = Switch_Beam;
      lpc_Switch_Beam = 0;
      last_Switch_Beam = Switch_Beam;
    }
  }

  // Brake debounce
  if (Switch_Brake != last_Switch_Brake) {
    lpc_Switch_Brake ++;
    if (lpc_Switch_Brake == 1) {
      lpc_check_Switch_Brake = loopcount;
    }

    if (lpc_Switch_Brake > 5) { //not 5 subsequent 
      lpc_Switch_Brake = 0;
    }
    if ((lpc_Switch_Brake == 5) && ((loopcount - lpc_check_Switch_Brake) == 4)) {
      Switch_Brake_db = Switch_Brake;
      lpc_Switch_Brake = 0;
      last_Switch_Brake = Switch_Brake;
    }
  }

  // Indicator_L debounce
  if (Switch_Indicator_L != last_Switch_Indicator_L) {
    lpc_Switch_Indicator_L ++;
    if (lpc_Switch_Indicator_L == 1) {
      lpc_check_Switch_Indicator_L = loopcount;
    }

    if (lpc_Switch_Indicator_L > 5) { //not 5 subsequent 
      lpc_Switch_Indicator_L = 0;
    }
    if ((lpc_Switch_Indicator_L == 5) && ((loopcount - lpc_check_Switch_Indicator_L) == 4)) {
      Switch_Indicator_L_db = Switch_Indicator_L;
      lpc_Switch_Indicator_L = 0;
      last_Switch_Indicator_L = Switch_Indicator_L;
    }
  }

  // Indicator_R debounce
  if (Switch_Indicator_R != last_Switch_Indicator_R) {
    lpc_Switch_Indicator_R ++;
    if (lpc_Switch_Indicator_R == 1) {
      lpc_check_Switch_Indicator_R = loopcount;
    }

    if (lpc_Switch_Indicator_R > 5) { //not 5 subsequent 
      lpc_Switch_Indicator_R = 0;
    }
    if ((lpc_Switch_Indicator_R == 5) && ((loopcount - lpc_check_Switch_Indicator_R) == 4)) {
      Switch_Indicator_R_db = Switch_Indicator_R;
      lpc_Switch_Indicator_R = 0;
      last_Switch_Indicator_R = Switch_Indicator_R;
    }
  }

  // Hazard_Indicator debounce
  if (Switch_Hazard_Indicator != last_Switch_Hazard_Indicator) {
    lpc_Switch_Hazard_Indicator ++;
    if (lpc_Switch_Hazard_Indicator == 1) {
      lpc_check_Switch_Hazard_Indicator = loopcount;
    }

    if (lpc_Switch_Hazard_Indicator > 5) {  //not 5 subsequent 
      lpc_Switch_Hazard_Indicator = 0;
    }
    if ((lpc_Switch_Hazard_Indicator == 5) && ((loopcount - lpc_check_Switch_Hazard_Indicator) == 4)) {
      Switch_Hazard_Indicator_db = Switch_Hazard_Indicator;
      lpc_Switch_Hazard_Indicator = 0;
      last_Switch_Hazard_Indicator = Switch_Hazard_Indicator;
    }
  }

  // Start debounce
  if (Switch_Start != last_Switch_Start) {
    lpc_Switch_Start ++;
    if (lpc_Switch_Start == 1) {
      lpc_check_Switch_Start = loopcount;
    }

    if (lpc_Switch_Start > 10) {  //not 5 subsequent 
      lpc_Switch_Start = 0;
    }
    if ((lpc_Switch_Start == 10) && ((loopcount - lpc_check_Switch_Start) == 9)) {
      Switch_Start_db = Switch_Start;
      lpc_Switch_Start = 0;
      last_Switch_Start = Switch_Start;
    }
  }

  // Horn debounce
  if (Switch_Horn != last_Switch_Horn) {
    lpc_Switch_Horn ++;
    if (lpc_Switch_Horn == 1) {
      lpc_check_Switch_Horn = loopcount;
    }

    if (lpc_Switch_Horn > 5) {  //not 5 subsequent 
      lpc_Switch_Horn = 0;
    }
    if ((lpc_Switch_Horn == 5) && ((loopcount - lpc_check_Switch_Horn) == 4)) {
      Switch_Horn_db = Switch_Horn;
      lpc_Switch_Horn = 0;
      last_Switch_Horn = Switch_Horn;
    }
  }

  // Reverse debounce
  if (Switch_Reverse != last_Switch_Reverse) {
    lpc_Switch_Reverse ++;
    if (lpc_Switch_Reverse == 1) {
      lpc_check_Switch_Reverse = loopcount;
    }

    if (lpc_Switch_Reverse > 5) { //not 5 subsequent 
      lpc_Switch_Reverse = 0;
    }
    if ((lpc_Switch_Reverse == 5) && ((loopcount - lpc_check_Switch_Reverse) == 4)) {
      Switch_Reverse_db = Switch_Reverse;
      lpc_Switch_Reverse = 0;
      last_Switch_Reverse = Switch_Reverse;
    }
  }

  //----All inputs debounced----

  // ACC
  if (Switch_ACC_db == HIGH) {
    digitalWrite(Relay_ACC1, RELAY_ON);// set the Relay ON
    digitalWrite(Relay_ACC2, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_ACC1, RELAY_OFF);// set the Relay OFF
    digitalWrite(Relay_ACC2, RELAY_OFF);// set the Relay OFF
  }

  // Ignition
  if (Switch_Ignition_db == HIGH) {
    digitalWrite(Relay_Ignition1, RELAY_ON);// set the Relay ON
    digitalWrite(Relay_Ignition2, RELAY_ON);// set the Relay ON
    digitalWrite(Relay_ACC1, RELAY_ON);// set the Relay ON
    digitalWrite(Relay_ACC2, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Ignition1, RELAY_OFF);// set the Relay OFF
    digitalWrite(Relay_Ignition2, RELAY_OFF);// set the Relay OFF
  }

  // Start
  if (Switch_Ignition_db == HIGH && Switch_Park_db == HIGH && Switch_Start_db == HIGH ) {
    digitalWrite(Relay_Start, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Start, RELAY_OFF);// set the Relay OFF
  }

  // Horn
  if (Switch_Horn_db == HIGH) {
    digitalWrite(Relay_Horn, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Horn, RELAY_OFF);// set the Relay OFF
  }

  // Beam (High beam)
  if (Switch_Ignition_db == HIGH && Switch_Beam_db == HIGH) {
    digitalWrite(Relay_Beam, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Beam, RELAY_OFF);// set the Relay OFF
  }

  // Reverse Light
  if (Switch_Ignition_db == HIGH && Switch_Reverse_db == HIGH) {
    digitalWrite(Relay_Reverse_Light, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Reverse_Light, RELAY_OFF);// set the Relay OFF
  }

  // Horn
  if (Switch_Horn_db == HIGH) {
    digitalWrite(Relay_Horn, RELAY_ON);// set the Relay ON
  }  else  {
    digitalWrite(Relay_Horn, RELAY_OFF);// set the Relay OFF
  }

  // Indicator Left
  if (Switch_Indicator_L_db == HIGH && Switch_Hazard_Indicator_db == LOW) {
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      if (indicatorState == HIGH) {
        indicatorState = LOW;
      } else {
        indicatorState = HIGH;
      }
      digitalWrite(Relay_Indicator_LF, indicatorState); //indicatorState HIGH or LOW        
      digitalWrite(Relay_Indicator_LR, indicatorState); //indicatorState HIGH or LOW
    }
  }
  //new addition 2021-09-10
  if (Switch_Indicator_L_db == LOW && Switch_Hazard_Indicator_db == LOW) {
    digitalWrite(Relay_Indicator_LF, RELAY_OFF); //indicatorState LOW
    digitalWrite(Relay_Indicator_LR, RELAY_OFF); //indicatorState LOW
  }

  // INDICATOR RIGHT
  if (Switch_Indicator_R_db == HIGH && Switch_Hazard_Indicator_db == LOW) {
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      if (indicatorState == HIGH) {
        indicatorState = LOW;
      } else {
        indicatorState = HIGH;
      }
      digitalWrite(Relay_Indicator_RF, indicatorState); //indicatorState HIGH or LOW
      digitalWrite(Relay_Indicator_RR, indicatorState); //indicatorState HIGH or LOW
    }
  }

  //new addition 2021-09-10
  if (Switch_Indicator_R_db == LOW && Switch_Hazard_Indicator_db == LOW) {
    digitalWrite(Relay_Indicator_RF, RELAY_OFF); //indicatorState LOW
    digitalWrite(Relay_Indicator_RR, RELAY_OFF); //indicatorState LOW
  }

  // Hazard Switch
  if (Switch_Hazard_Indicator_db == HIGH && Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == LOW) {
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      if (indicatorState == HIGH) {
        indicatorState = LOW;
      } else {
        indicatorState = HIGH;
      }
      digitalWrite(Relay_Indicator_LF, indicatorState); //indicatorState HIGH or LOW
      digitalWrite(Relay_Indicator_LR, indicatorState); //indicatorState HIGH or LOW
      digitalWrite(Relay_Indicator_RF, indicatorState); //indicatorState HIGH or LOW
      digitalWrite(Relay_Indicator_RR, indicatorState); //indicatorState HIGH or LOW
    }
  }

  if (Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == LOW && Switch_Hazard_Indicator_db == LOW) {
    digitalWrite(Relay_Indicator_LF, RELAY_OFF);
    digitalWrite(Relay_Indicator_LR, RELAY_OFF);
  }
  if (Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == LOW && Switch_Hazard_Indicator_db == LOW && Switch_Brake_db == LOW) {  
    digitalWrite(Relay_Indicator_RF, RELAY_OFF);
    digitalWrite(Relay_Indicator_RR, RELAY_OFF);
  }

  // Brake Switch
  if (Switch_Brake_db == HIGH && Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == LOW) {
    digitalWrite(Relay_Indicator_LR, RELAY_ON);
    digitalWrite(Relay_Indicator_RR, RELAY_ON);
  }
  if (Switch_Brake_db == HIGH && Switch_Indicator_L_db == HIGH && Switch_Indicator_R_db == LOW) {
    digitalWrite(Relay_Indicator_RR, RELAY_ON);
  }
  if (Switch_Brake_db == LOW && Switch_Indicator_L_db == HIGH && Switch_Indicator_R_db == LOW) {
    digitalWrite(Relay_Indicator_RR, RELAY_OFF);
  } 
  if (Switch_Brake_db == HIGH && Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == HIGH) {
    digitalWrite(Relay_Indicator_LR, RELAY_ON);
  }
  if (Switch_Brake_db == LOW && Switch_Indicator_L_db == LOW && Switch_Indicator_R_db == HIGH) {
    digitalWrite(Relay_Indicator_LR, RELAY_OFF);
  }


/*****************Bluetooth Module Functions***************/

/*
    Different states from Bluetooth app.
    1 Relay Ignition ON
    2 Relay Ignition OFF
    3 Relay Start ON
    4 Relay Start OFF
    5 Relay Beam ON
    6 Relay Beam OFF
  */
	//if some data is sent, read it and save it in the state variable

//uncomment bluetooth module due to possible EMI-fault
/* 
	if (Serial3.available() > 0) {
		state = Serial3.read();
	}

	//CRAZY BANANAS
	if (state == '1') {
		digitalWrite(Relay_Beam, RELAY_ON); //tuuuut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(200);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(500);
		digitalWrite(Relay_Beam, RELAY_ON);  //tut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(50);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(100);
		digitalWrite(Relay_Beam, RELAY_ON);  //tut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(50);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(150);
		digitalWrite(Relay_Beam, RELAY_ON);  //tut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(50);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(150);
		digitalWrite(Relay_Beam, RELAY_ON);  //tut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(50);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(500);
		digitalWrite(Relay_Beam, RELAY_ON);  //tuuut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(200);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(500);
		digitalWrite(Relay_Beam, RELAY_ON);  //tuuut
		digitalWrite(Relay_Horn, RELAY_ON);
		delay(200);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		delay(10000);
		Serial.println("crazy");
	}

	if (state == '2') {
		digitalWrite(Relay_Indicator_LF, RELAY_OFF);
		digitalWrite(Relay_Beam, RELAY_OFF);
		digitalWrite(Relay_Horn, RELAY_OFF);
		digitalWrite(Relay_Indicator_RF, RELAY_OFF);
		Serial.println("crazy OFF");
	}

	//START
	if (state == '3') {
		digitalWrite(Relay_Start, RELAY_ON);
		Serial.println("Start Relay ON");
	}

	if (state == '4') {
		digitalWrite(Relay_Start, RELAY_OFF);
		Serial.println("Start Relay OFF");
	}

	//BEAM
	if (state == '5') {
		digitalWrite(Relay_Beam, RELAY_ON);
		Serial.println("Beam ON");
	}

	if (state == '6') {
		digitalWrite(Relay_Beam, RELAY_OFF);
		Serial.println("Beam OFF");
	}

	//HORN
	if (state == '7') {
		digitalWrite(Relay_Horn, RELAY_ON);
		Serial.println("Horn ON");
	}

	if (state == '8') {
		digitalWrite(Relay_Horn, RELAY_OFF);
		Serial.println("Horn OFF");
	}

*/

	//For screen

	lpc_display++; //loopcount display
	if (lpc_display >= 200)  { // loopdelay = 10ms, 0.01*200 = 2s, update LCD every other second.
		lpc_display = 0;
		if (Switch_ACC_db == HIGH) {
			tft.setCursor(130, 80);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print("OFF");
			tft.setCursor(130, 80);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print("ON");
		}
		if (Switch_ACC_db == LOW) {
			tft.setCursor(130, 80);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print("ON");
			tft.setCursor(130, 80);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print("OFF");
		}

		if (Switch_Ignition_db == HIGH) {
			tft.setCursor(130, 90);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(0);
			tft.setCursor(130, 90);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(1);
		} else {
			tft.setCursor(130, 90);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(1);
			tft.setCursor(130, 90);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(0);
		}

		if (Switch_Park_db == HIGH) {
			tft.setCursor(130, 100);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(0);
			tft.setCursor(130, 100);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(1);
		} else {
			tft.setCursor(130, 100);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(1);
			tft.setCursor(130, 100);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(0);
		}

		if (Switch_Beam_db == HIGH) {
			tft.setCursor(130, 110);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(0);
			tft.setCursor(130, 110);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(1);
		} else {
			tft.setCursor(130, 110);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(1);
			tft.setCursor(130, 110);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(0);
		}

		if (Switch_Brake_db == HIGH) {
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(0);
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(1);
		} else {
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(1);
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(0);
		}

		if (Switch_Indicator_L_db == HIGH) {
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(0);
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(1);
		} else {
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_BLACK);  tft.setTextSize(1);
			tft.print(1);
			tft.setCursor(130, 120);
			tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
			tft.print(0);
		}

		tft.setCursor(130, 120);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Indicator_L_db);

		tft.setCursor(130, 130);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Indicator_R_db);

		tft.setCursor(130, 140);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Hazard_Indicator_db);

		tft.setCursor(130, 150);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Start_db);

		tft.setCursor(130, 150);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Horn_db);

		tft.setCursor(130, 160);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(Switch_Reverse_db);

		tft.setCursor(130, 190);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(24));

		tft.setCursor(130, 200);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(25));

		tft.setCursor(130, 210);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(26));

		tft.setCursor(130, 220);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(27));

		tft.setCursor(130, 230);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(28));

		tft.setCursor(130, 240);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(29));

		tft.setCursor(130, 250);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(30));

		tft.setCursor(130, 260);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(31));

		tft.setCursor(130, 270);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(32));

		tft.setCursor(130, 280);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(33));

		tft.setCursor(130, 290);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(34));

		tft.setCursor(130, 300);
		tft.setTextColor(HX8357_WHITE);  tft.setTextSize(1);
		tft.print(digitalRead(35));
	}
	delay(loopdelay);
}

//--(end main loop )---

//*********( THE END )***********
